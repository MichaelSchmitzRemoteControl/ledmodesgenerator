#ifndef LEDMODESGENERATOR_DEFINITIONS_H
#define LEDMODESGENERATOR_DEFINITIONS_H

#include <string>

typedef unsigned int COLORREF;
typedef unsigned char   BYTE;
#define RGB(r, g, b)    (((unsigned char)r) | ((unsigned char)g << 8) | ((unsigned char)b << 16))
#define GetRValue(rgb)  ((unsigned char) (rgb))
#define GetGValue(rgb)  ((unsigned char) ((rgb) >> 8))
#define GetBValue(rgb)  ((unsigned char) ((rgb) >> 16))
#define GetRValueString(rgb)  (std::to_string((unsigned char) (rgb)))
#define GetGValueString(rgb)  (std::to_string((unsigned char) ((rgb) >> 8)))
#define GetBValueString(rgb)  (std::to_string((unsigned char) ((rgb) >> 16)))
#define THREAD void*
#define THREADRETURN return(NULL);
enum Mode {
    rainbow, rainbowSinus, colorWheel, cycleSinus, cycle, undefined
};
typedef struct
{
    COLORREF pixels[64][256];
} vis_pixels;
#endif
