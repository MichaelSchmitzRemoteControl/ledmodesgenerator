#ifndef MODE_GENERATOR_H
#define MODE_GENERATOR_H

#include <math.h>
#include <boost/function.hpp>
#include <pthread.h>
#include "hsv.h"
#include "definitions.h"
#include "sendToArduino.h"
#include <sys/stat.h>

class ModeGenerator
{
public:
    ModeGenerator(int leds, Mode mode, std::string ip, int brightness, float speed, __useconds_t delay);
    void stop();

    vis_pixels pixels;

    vis_pixels pixels1;

    vis_pixels pixels2;

    vis_pixels *pixels_out;

    vis_pixels *pixels_render;
private:
    sendToArduino* strip;
    float step;
    std::string ip;
    __useconds_t delay;
    float speed;
    int brightness;
    bool running;
    boost::function<void(ModeGenerator *)> modeFunction;

    static THREAD VisThread(void* param);
    static THREAD LEDStripUpdateThread(void* param);
    static THREAD FileWatcher(void* param);
    boost::function<void(ModeGenerator *)> getModeFunction(Mode mode);
    void StartThread();
    void DrawSinusoidalCycle();
    void DrawSpectrumCycle();
    void DrawColorWheel();
    void DrawRainbow();
    void DrawRainbowSinusoidal();
};
#endif