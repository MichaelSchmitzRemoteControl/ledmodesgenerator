#include "ModeGenerator.h"

ModeGenerator::ModeGenerator(int _leds, Mode _mode, std::string _ip, int _brightness, float _speed, __useconds_t _delay)
{
    ip = _ip;
    strip = new sendToArduino(_leds, _ip);
    modeFunction = getModeFunction(_mode);
    brightness = (int) (_brightness * (255.0f / 100.0f));
    speed = _speed;
    delay = _delay;
    step = 0;
    pixels_render = &pixels1;
    pixels_out = &pixels2;
    StartThread();
}

void ModeGenerator::stop() {
    running = false;
    strip->stop();
}

void ModeGenerator::StartThread() {
    running = true;
    pthread_t threads[3];
    pthread_create(&threads[0], nullptr, &VisThread, this);
    pthread_create(&threads[1], nullptr, &LEDStripUpdateThread, this);
    pthread_create(&threads[2], nullptr, &FileWatcher, this);
    pthread_join(threads[0], nullptr);
    pthread_join(threads[1], nullptr);
    pthread_join(threads[2], nullptr);
}

void ModeGenerator::DrawRainbowSinusoidal() {
    for (int x = 0; x < 256; x++) {
        for (int y = 0; y < 64; y++) {
            int red = (int) (127 *
                             (sin(((((int) ((x * (360 / 255.0f)) - step) % 360) / 360.0f) * 2 * 3.14f)) + 1));
            int grn = (int) (127 * (sin(((((int) ((x * (360 / 255.0f)) - step) % 360) / 360.0f) * 2 * 3.14f) -
                                        (6.28f / 3)) + 1));
            int blu = (int) (127 * (sin(((((int) ((x * (360 / 255.0f)) - step) % 360) / 360.0f) * 2 * 3.14f) +
                                        (6.28f / 3)) + 1));
            pixels.pixels[y][x] = RGB(((brightness * red) / 256), ((brightness * grn) / 256), ((brightness * blu) / 256));
        }
    }
}

void ModeGenerator::DrawRainbow() {
    for (int x = 0; x < 256; x++) {
        for (int y = 0; y < 64; y++) {
            int hsv_h = ((int) (step + (256 - x)) % 360);
            hsv_t hsv = {hsv_h, 255, (unsigned char) brightness};
            pixels.pixels[y][x] = hsv2rgb(&hsv);
        }
    }
}

void ModeGenerator::DrawColorWheel() {
    for (int x = 0; x < 256; x++) {
        for (int y = 0; y < 64; y++) {
            float hue = (step + (int) (180 + atan2(y - 128, x - 32) * (180.0 / 3.14159)) % 360);
            hsv_t hsv2 = {(int) hue, 255, (unsigned char) brightness};
            pixels.pixels[y][x] = hsv2rgb(&hsv2);
        }
    }
}

void ModeGenerator::DrawSpectrumCycle() {
    hsv_t hsv2 = {(int) step, 255, (unsigned char) brightness};
    COLORREF color = hsv2rgb(&hsv2);

    for (int x = 0; x < 256; x++) {
        for (int y = 0; y < 64; y++) {
            pixels.pixels[y][x] = color;
        }
    }
}

void ModeGenerator::DrawSinusoidalCycle() {
    COLORREF color;
    int red = (int) (127 * (sin(((((int) (((360 / 255.0f)) - step) % 360) / 360.0f) * 2 * 3.14f)) + 1));
    int grn = (int) (127 *
                     (sin(((((int) (((360 / 255.0f)) - step) % 360) / 360.0f) * 2 * 3.14f) - (6.28f / 3)) + 1));
    int blu = (int) (127 *
                     (sin(((((int) (((360 / 255.0f)) - step) % 360) / 360.0f) * 2 * 3.14f) + (6.28f / 3)) + 1));
    color = RGB(((brightness * red) / 256), ((brightness * grn) / 256), ((brightness * blu) / 256));

    for (int x = 0; x < 256; x++) {
        for (int y = 0; y < 64; y++) {
            pixels.pixels[y][x] = color;
        }
    }
}

boost::function<void(ModeGenerator *)> ModeGenerator::getModeFunction(Mode mode) {
    switch (mode) {
        case rainbow: {
            return &ModeGenerator::DrawRainbow;
        }
        case rainbowSinus: {
            return &ModeGenerator::DrawRainbowSinusoidal;
        }
        case colorWheel: {
            return &ModeGenerator::DrawColorWheel;
        }
        case cycle: {
            return &ModeGenerator::DrawSpectrumCycle;
        }
        case cycleSinus: {
            return &ModeGenerator::DrawSinusoidalCycle;
        }
        default: {
            return &ModeGenerator::DrawColorWheel;
        }
    }
}

THREAD ModeGenerator::VisThread(void* param) {
    auto *instance = static_cast<ModeGenerator *>(param);
    while (instance->running) {
        if (instance->step >= 360.0f) instance->step = 0.0f;
        if (instance->step < 0.0f) instance->step = 360.0f;

        instance->modeFunction(instance);

        for (int x = 0; x < 256; x++) {
            for (int y = 0; y < 64; y++) {
                instance->pixels_render->pixels[y][x] = instance->pixels.pixels[y][x];
            }
        }

        //Swap buffers
        if (instance->pixels_render == &instance->pixels1) {
            instance->pixels_render = &instance->pixels2;
            instance->pixels_out = &instance->pixels1;
        } else {
            instance->pixels_render = &instance->pixels1;
            instance->pixels_out = &instance->pixels2;
        }

        instance->step = instance->step += (instance->speed / 100.0f);

        usleep(15 * 1000);
    }
}

THREAD ModeGenerator::LEDStripUpdateThread(void* param) {
    auto *instance = static_cast<ModeGenerator *>(param);
    while (instance->running) {
        instance->strip->SetPixels(instance->pixels_out->pixels);
        instance->strip->SetDelay(instance->delay);

        usleep(instance->delay * 1000);
    }
}

THREAD ModeGenerator::FileWatcher(void* param) {
    auto *instance = static_cast<ModeGenerator *>(param);
    while(instance->running) {
        struct stat buffer;
        if (stat (("/tmp/LedModesGenerator" + instance->ip).c_str(), &buffer) == 0) {
            instance->stop();
            remove(("/tmp/LedModesGenerator" + instance->ip).c_str());
        }
    }
}
