
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <Adafruit_NeoPixel.h>

#define PIN 13
#define LEDS 28


// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(LEDS, PIN, NEO_RGB + NEO_KHZ800);

ESP8266WebServer server(80);
void setup() {
  Serial.begin(115200);
  setupWifi("Home", "Der_Hobbit123");
  
  server.on("/", handleInit);
  server.on("/color", handleColor);
  server.on("/mode", handleMode);

  server.onNotFound(handleNotFound);

  server.begin();
  strip.begin();
  delay(500);
  handleInit();
}

void loop() {
  server.handleClient();
}

void setupWifi(char* ssid, char* pwd) {
  Serial.println("Connecting to WiFi");
  WiFi.begin(ssid, pwd);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("connected");
}

void setColor(int red, int green, int blue) {
  for(int i = 0; i < LEDS; i++) {
    strip.setPixelColor(i, strip.Color(red, green, blue));
  }
  strip.show();
}

void setMode(String allColors) {
  for (int i = 0; i < LEDS; i++) {
    String colors = allColors.substring(i * 9, (i + 1) * 9);
    strip.setPixelColor(i, strip.Color(colors.substring(0, 3).toInt(), colors.substring(3, 6).toInt(), colors.substring(6, 9).toInt()));
  }
  Serial.println(allColors.substring(3,6).toInt());
  strip.show();
}

void handleInit() {
  if (server.method() == HTTP_PUT) {
    setColor(31, 59, 242);
    server.send(200);  
  }
}

void handleColor() {
  if (server.method() == HTTP_PUT && server.hasArg("red") && server.hasArg("blue") && server.hasArg("green")) {
    int red = server.arg("red").toInt();
    int blue = server.arg("blue").toInt();
    int green = server.arg("green").toInt();
    setColor(red, green, blue);
    server.send(200);
  } else {
    server.send(400);
  }
}

void handleMode() {
  if (server.method() == HTTP_PUT && server.hasArg("allColors") && server.arg("allColors").length() == LEDS * 9) {
    setMode(server.arg("allColors"));
    server.send(200);
  } else {
    server.send(400);
  }
}

void handleNotFound() {
  server.send(400);
}
