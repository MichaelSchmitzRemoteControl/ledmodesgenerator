#include <iostream>
#include <boost/program_options.hpp>
#include <netinet/in.h>
#include <libnet.h>
#include <pthread.h>
#include <fstream>
#include "ModeGenerator.h"

namespace po = boost::program_options;
using namespace std;

void run(int leds, int brightness, string ip, Mode mode, int delay, __useconds_t speed) {
    new ModeGenerator(leds, mode, move(ip), brightness, delay, speed);
}

int main(int argc, char **argv) {
    Mode mode = undefined;
    int leds = -1;
    int brightness = -1;
    int delay = 50;
    __useconds_t speed = 250;
    string ip;
    string kill;

    po::options_description desc("Usage:");
    desc.add_options()
            ("help", "show this message")
            ("mode", po::value<string>(),
             "set mode, has to be one of {rainbow, rainbowSinus, colorWheel, cycleSinus, cycle}")
            ("brightness", po::value<int>(), "set brightness, between 0 and 100")
            ("leds", po::value<int>(), "set number of leds, has to be greater 0")
            ("ip", po::value<string>(), "set ip and port, format: 123.4.56.43[:80]")
            ("delay", po::value<int>(), "set delay (number, optional)")
            ("speed", po::value<__useconds_t>(), "set Animation Speed (number, optional)")
            ("kill", po::value<string>(), "all other flags should not be present, value should be like ip");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        cout << desc << endl;
        return 1;
    }
    if(vm.count("kill")) {
        string tmpIP = vm["kill"].as<string>();
        std::ofstream outfile ("/tmp/LedModesGenerator" + tmpIP);
        outfile << endl;
        outfile.close();
        return 0;
    }
    if (vm.count("mode")) {
        string tmpMode = vm["mode"].as<string>();
        if (tmpMode == "rainbow") {
            mode = rainbow;
        } else if (tmpMode == "rainbowSinus") {
            mode = rainbowSinus;
        } else if (tmpMode == "colorWheel") {
            mode = colorWheel;
        } else if (tmpMode == "cycleSinus") {
            mode = cycleSinus;
        } else if (tmpMode == "cycle") {
            mode = cycle;
        } else {
            cout << "Mode has wrong format" << endl << desc << endl;
            return 1;
        }
    } else {
        cout << "Mode was not set." << endl;
    }
    if (vm.count("leds")) {
        int tmpLeds = vm["leds"].as<int>();
        if (tmpLeds > 0) {
            leds = tmpLeds;
        } else {
            cout << "Number of Leds has to be greater than 0" << endl << desc << endl;
            return 1;
        }
    } else {
        cout << "Number of Leds was not set." << endl;
    }
    if (vm.count("brightness")) {
        int tmpBrightness = vm["brightness"].as<int>();
        if (tmpBrightness >= 0 && tmpBrightness <= 100) {
            brightness = tmpBrightness;
        } else {
            cout << "Brightness has to be between 0 and 100" << endl << desc << endl;
            return 1;
        }
    } else {
        cout << "Brightness was not set." << endl;
    }
    if (vm.count("ip")) {
        //TODO validate IP
        string tmpIp = vm["ip"].as<string>();
        ip = tmpIp;

    } else {
        cout << "Ip not set." << endl;
    }
    if (mode == undefined || leds == -1 || brightness == -1 || ip.empty()) {
        cout << "Please set all parameters" << endl;
        return 1;
    }
    if (vm.count("delay")) {
        int tmpDelay = vm["delay"].as<int>();
        if (tmpDelay > 0) {
            delay = tmpDelay;
        } else {
            cout << "Delay has to be greater than 0" << endl << desc << endl;
            return 1;
        }
    }
    if (vm.count("speed")) {
        __useconds_t tmpSpeed = vm["speed"].as<__useconds_t>();
        if (tmpSpeed > 0) {
            speed = tmpSpeed;
        } else {
            cout << "Animation Speed has to be greater than 0" << endl << desc << endl;
            return 1;
        }
    }
    int i = fork();
    if (i < 0) exit(1); /* fork error */
    if (i > 0) exit(0);
    run(leds, brightness, ip, mode, delay, speed);
    return 0;
}