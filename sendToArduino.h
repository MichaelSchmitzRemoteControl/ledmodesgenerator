#ifndef SEND_TO_ARDUINO_H
#define SEND_TO_ARDUINO_H

#include "definitions.h"

class sendToArduino
{
public:
    sendToArduino(int numleds, std::string ip);
    void stop();
    void SetPixels(COLORREF pixels[64][256]);
    void SetDelay(__useconds_t delay);

private:
    bool running;
    int numLeds;
    std::string ip;
    __useconds_t led_delay = 10;

    int * ledIndex;
    COLORREF (*led_pixels)[256] = NULL;

    void Initialize();
    void getPixelPositions();
    static THREAD lsthread(void *param);
    void LEDStripUpdateThread();
    void SetLEDs(COLORREF pixels[64][256]);
};

#endif
