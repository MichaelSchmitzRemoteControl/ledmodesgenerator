#include "sendToArduino.h"

#include <string>
#include <cstring>
#include <curl/curl.h>

#include "pthread.h"
#include "unistd.h"

THREAD sendToArduino::lsthread(void *param) {
    auto *led = static_cast<sendToArduino *>(param);
    led->LEDStripUpdateThread();
    THREADRETURN
}

sendToArduino::sendToArduino(int _numLeds, std::string _ip) {
    numLeds = _numLeds;
    ip = move(_ip);
    Initialize();
}

void sendToArduino::stop() {
    running = false;
}

void sendToArduino::Initialize() {
    running = true;
    getPixelPositions();
    pthread_t threads[1];
    pthread_create(&threads[0], nullptr, &lsthread, this);
}

void sendToArduino::getPixelPositions() {
    ledIndex = new int[numLeds];

    if ((numLeds % 2) == 0) {
        {
            for (int i = 0; i < numLeds; i++) {
                ledIndex[i] = (int) ((i * (256.0f / (numLeds - 1))));

                /*    if (i == (numLeds - 1))
                    {
                        ledIndex[i] = ledIndex[i] - 1;
                    }
    */
            }
        }
    } else {
        for (int i = 0; i < numLeds; i++) {
            if (i == (numLeds / 2)) {
                ledIndex[i] = 128;
            } else if (i < ((numLeds / 2) + 1)) {
                ledIndex[i] = (numLeds / 2) + ((i + 1) * (256 / (numLeds + 1)));
            } else {
                ledIndex[i] = ((numLeds / 2) + 1) + (i * (256 / (numLeds + 1)));
            }
        }
    }
}

std::string padRGB(std::string color) {
    if (color.length() == 1) {
        return "00" + color;
    } else if (color.length() == 2) {
        return "0" + color;
    } else {
        return color;
    }
}

std::string rgb2string(COLORREF rgb) {
    return padRGB(GetRValueString(rgb)) + padRGB(GetGValueString(rgb)) + padRGB(GetBValueString(rgb));
}

void sendToArduino::SetPixels(COLORREF pixels[64][256]) {
    led_pixels = pixels;
}

void sendToArduino::SetLEDs(COLORREF pixels[64][256]) {
    std::string colorString;
    for (int i = 0; i < numLeds; i++) {
        colorString += rgb2string(pixels[0][ledIndex[i]]);


    }
    auto curl = curl_easy_init();

    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, ("http://" + ip + "/mode?allColors=" + colorString).c_str());
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");

        curl_easy_perform(curl);

        curl_easy_cleanup(curl);
    }
}

void sendToArduino::SetDelay(__useconds_t delay) {
    led_delay = delay;
}

void sendToArduino::LEDStripUpdateThread() {
    while (running) {
        if (led_pixels != nullptr) {
            SetLEDs(led_pixels);
        }

        usleep(led_delay * 1000);
    }
}